# base-project

#### 项目介绍
基础后台管理系统模板项目代码
前后端分离
前端vue构建 主要:vue+elementui
后端springboot+mybatis


演示地址http://120.79.5.163:8077/

用户名：admin 密码：123456

#### 软件架构

### 后端:


主体架构：springboot 2.1.0

数据库持久层:mybatis-plus

数据库连接池:druid

代码生成器:mybatis-plus (里面封装了可生成vue的.vue单文件组件以及对应的js)
生成的代码按照填写的模块名直接写入到项目目录上。前端的vue和js文件需要手动拷贝到前端项目对应得位置上，并且添加路由即可

安全认证权限框架：shiro

关系型数据库:mysql

缓存：redis

文件服务器：seaweedfs


### 前端:


vue-cli、vue-router、vuex、elementui等

这里使用的是https://panjiachen.github.io/vue-element-admin-site/zh/guide/

提供的基础模板，并在基础模板上增加了一些集成方案上的功能。

页面权限校验与动态路由功能、角色分配、权限分配使用了springboot-hichat的功能 https://gitee.com/gaojunjie03/springboot-hichat

#### 环境

1. java1.8
2. maven
3. idea+webstorm
4. nodejs

#### 部署说明

1. api项目jar方式打包部署 linux执行命令 nohup java -jar base.jar & >> base.out
2. html前端打包后放到nginx的html文件夹下，由nginx代理
3. nginx代理api接口
4. 借nginx的反向代理解决前后端分离的跨域问题


#### 文件服务器seaweedfs

SeaweedFS是一种简单的、高度可扩展的分布式文件系统。有两个目标:

1）存储数十亿的文件!

2）查看档案快!

SeaweedFS最初作为一个对象存储来有效地处理小文件。中央主服务器只管理文件卷，而不是管理中央主服务器中的所有文件元数据，它允许这些卷服务器管理文件及其元数据。这减轻了中央主服务器的并发压力，并将文件元数据传播到卷服务器，允许更快的文件访问(只需一个磁盘读取操作)。

下载地址:https://github.com/chrislusf/seaweedfs/releases

官方文档:https://github.com/chrislusf/seaweedfs

由于seaweedfs是go语言编写的，所以在安装seaweedfs前需要安装go的环境

下载好seaweedfs的tar.gz后放到linux服务器解压

tar -zxvf seaweedfs.tar.gz

会得到一个文件weed，然后在任意目录下创建seaweedfs存放文件的目录 如usr/local/weeddata1、usr/local/weeddata1

最后启动seaweedfs只需启动文件weed

启动master

./weed master -ip=127.0.0.1 -port=9333

启动volume (如两个节点)

./weed volume -dir=/usr/local/weeddata1 -mserver=127.0.0.1:9333 -ip=127.0.0.1 -port=8080

./weed volume -dir=/usr/local/weeddata2 -mserver=127.0.0.1:9333 -ip=127.0.0.1 -port=8081

启动成功后可访问查看127.0.0.1:9333  查看seaweedfs后台

QQ：807758751
