/*
 Navicat Premium Data Transfer

 Source Server         : 120.79.5.163_3306
 Source Server Type    : MySQL
 Source Server Version : 80013
 Source Host           : 120.79.5.163:3306
 Source Schema         : base

 Target Server Type    : MySQL
 Target Server Version : 80013
 File Encoding         : 65001

 Date: 08/12/2018 14:55:37
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for COMPANY_INFO
-- ----------------------------
DROP TABLE IF EXISTS `COMPANY_INFO`;
CREATE TABLE `COMPANY_INFO`  (
  `ID` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `NAME` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司名',
  `ADDRESS` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司地址',
  `CORPORATION` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司法人',
  `REGISTRY_MONEY` double(10, 2) NULL DEFAULT NULL COMMENT '注册资金',
  `TELEPHONE` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '固定电话',
  `CREATE_TIME` datetime(0) NULL DEFAULT NULL COMMENT '创建时间 ',
  `UPDATE_TIME` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `VERSION` int(10) NULL DEFAULT NULL COMMENT '版本',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for SYS_DICT
-- ----------------------------
DROP TABLE IF EXISTS `SYS_DICT`;
CREATE TABLE `SYS_DICT`  (
  `ID` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典名称',
  `VALUE` int(10) NULL DEFAULT NULL COMMENT '字典值',
  `DESCRIPTION` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `TYPE` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典类型',
  `VERSION` int(10) NULL DEFAULT NULL COMMENT '版本号',
  `CREATE_TIME` datetime(0) NULL DEFAULT NULL,
  `UPDATE_TIME` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of SYS_DICT
-- ----------------------------
INSERT INTO `SYS_DICT` VALUES ('11789', '审核通过', 2, '审核状态', 'auditStatus', 1, '2018-12-05 09:56:50', NULL);
INSERT INTO `SYS_DICT` VALUES ('123', '是', 1, '是否默认', 'isOrNo', 1, '2018-12-05 09:57:15', NULL);
INSERT INTO `SYS_DICT` VALUES ('456', '未审核', 1, '审核状态', 'auditStatus', 1, '2018-12-05 09:57:18', NULL);
INSERT INTO `SYS_DICT` VALUES ('4567', '审核不通过', 3, '审核状态', 'auditStatus', 1, '2018-12-05 09:57:12', NULL);
INSERT INTO `SYS_DICT` VALUES ('5648', '否', 2, '是否默认', 'isOrNo', 1, '2018-12-05 09:56:58', NULL);
INSERT INTO `SYS_DICT` VALUES ('7d1cd1bb7fbc4961a91f5cb6d393dbb8', '禁用', 2, '启用禁用', 'useStatus', 1, '2018-12-05 09:57:21', NULL);
INSERT INTO `SYS_DICT` VALUES ('e774004790144a2b98fcac329a364f1a', '启用', 1, '启用禁用', 'useStatus', 3, '2018-12-05 09:57:03', NULL);
INSERT INTO `SYS_DICT` VALUES ('f105e6ad30b849ce9980e60243c67639', '女', 2, '性别', 'sex', 1, '2018-12-06 10:31:27', NULL);
INSERT INTO `SYS_DICT` VALUES ('fba34f84a72c46c9b7b297923aeeae81', '男', 1, '性别', 'sex', 1, '2018-12-05 17:37:21', NULL);

-- ----------------------------
-- Table structure for SYS_PERMISSION
-- ----------------------------
DROP TABLE IF EXISTS `SYS_PERMISSION`;
CREATE TABLE `SYS_PERMISSION`  (
  `ID` int(11) NOT NULL COMMENT '自定id,主要供前端展示权限列表分类排序使用.',
  `MENU_CODE` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '归属菜单,前端判断并展示菜单使用,',
  `MENU_NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '菜单的中文释义',
  `PERMISSION_CODE` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '权限的代码/通配符,对应代码中@RequiresPermissions 的value',
  `PERMISSION_NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '本权限的中文释义',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of SYS_PERMISSION
-- ----------------------------
INSERT INTO `SYS_PERMISSION` VALUES (8, 'employee', '系统用户管理', 'employee:query', '查询');
INSERT INTO `SYS_PERMISSION` VALUES (9, 'employee', '系统用户管理', 'employee:edit', '编辑');
INSERT INTO `SYS_PERMISSION` VALUES (11, 'employee', '系统用户管理', 'employee:delete', '删除');
INSERT INTO `SYS_PERMISSION` VALUES (12, 'employee', '系统用户管理', 'employee:reset', '重置密码');
INSERT INTO `SYS_PERMISSION` VALUES (13, 'employee', '系统用户管理', 'employee:assign', '分配角色');
INSERT INTO `SYS_PERMISSION` VALUES (14, 'role', '系统角色管理', 'role:query', '角色管理查询');
INSERT INTO `SYS_PERMISSION` VALUES (15, 'role', '系统角色管理', 'role:edit', '编辑');
INSERT INTO `SYS_PERMISSION` VALUES (17, 'role', '系统角色管理', 'role:delete', '删除');
INSERT INTO `SYS_PERMISSION` VALUES (18, 'role', '系统角色管理', 'role:assign', '分配权限');
INSERT INTO `SYS_PERMISSION` VALUES (19, 'dict', '系统数据字典', 'dict:query', '查询');
INSERT INTO `SYS_PERMISSION` VALUES (20, 'dict', '系统数据字典', 'dict:edit', '编辑');
INSERT INTO `SYS_PERMISSION` VALUES (21, 'employee', '系统用户管理', 'employee:export', '导出');
INSERT INTO `SYS_PERMISSION` VALUES (22, 'employee', '系统用户管理', 'employee:import', '导入');
INSERT INTO `SYS_PERMISSION` VALUES (23, 'employee', '系统用户管理', 'employee:useStatus', '启用禁用');
INSERT INTO `SYS_PERMISSION` VALUES (24, 'employee', '系统用户管理', 'employee:log', '日志');
INSERT INTO `SYS_PERMISSION` VALUES (25, 'code', '代码生成', 'code:query', '代码生成');

-- ----------------------------
-- Table structure for SYS_ROLE
-- ----------------------------
DROP TABLE IF EXISTS `SYS_ROLE`;
CREATE TABLE `SYS_ROLE`  (
  `ID` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ROLE_NAME` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色名称',
  `CREATE_DATE` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `UPDATE_DATE` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `VERSION` int(10) NULL DEFAULT NULL COMMENT '版本',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of SYS_ROLE
-- ----------------------------
INSERT INTO `SYS_ROLE` VALUES ('1', '超级管理员', '2018-11-30 12:00:00', '2018-11-29 13:00:00', 1);

-- ----------------------------
-- Table structure for SYS_ROLE_PERMISSION
-- ----------------------------
DROP TABLE IF EXISTS `SYS_ROLE_PERMISSION`;
CREATE TABLE `SYS_ROLE_PERMISSION`  (
  `ID` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ROLE_ID` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色id',
  `PERMISSION_ID` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限id',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of SYS_ROLE_PERMISSION
-- ----------------------------
INSERT INTO `SYS_ROLE_PERMISSION` VALUES ('0d9d427d78174b9986b7b140df719e48', '1', '9');
INSERT INTO `SYS_ROLE_PERMISSION` VALUES ('0ebc5527ad5f44b68b8d347081000b56', '1', '11');
INSERT INTO `SYS_ROLE_PERMISSION` VALUES ('121919c896434f2db5bd1387311b9de7', '1', '23');
INSERT INTO `SYS_ROLE_PERMISSION` VALUES ('23995b35f232421997f8eecbf75747bb', '1', '22');
INSERT INTO `SYS_ROLE_PERMISSION` VALUES ('2d499a9865cf4503918d5fa0e2cc5e82', '1', '17');
INSERT INTO `SYS_ROLE_PERMISSION` VALUES ('335d348dcbf64b9094d44f6d03ae368d', '1', '8');
INSERT INTO `SYS_ROLE_PERMISSION` VALUES ('553cbebd983d496a9679968e83ad0c0f', '1', '18');
INSERT INTO `SYS_ROLE_PERMISSION` VALUES ('5e44a3fd38264428bf0b5139b4d0ec65', '1', '21');
INSERT INTO `SYS_ROLE_PERMISSION` VALUES ('614ea07bbbcf4770840eff07812feec8', '1', '13');
INSERT INTO `SYS_ROLE_PERMISSION` VALUES ('75866a9ea4e149399e3540f4ed000e4f', '1', '12');
INSERT INTO `SYS_ROLE_PERMISSION` VALUES ('8c4852516712443cbdaf75bec47ca4d1', '1', '15');
INSERT INTO `SYS_ROLE_PERMISSION` VALUES ('92e452e50eb6416db6135e0c4e28dc69', '1', '20');
INSERT INTO `SYS_ROLE_PERMISSION` VALUES ('a91bab8759414bde9705933ffcf5366d', '1', '24');
INSERT INTO `SYS_ROLE_PERMISSION` VALUES ('c0115d7aa38f4a079f24d4923bd90f3c', '1', '19');
INSERT INTO `SYS_ROLE_PERMISSION` VALUES ('df562484d4494c548bb5c4bde7c79f9d', '1', '25');
INSERT INTO `SYS_ROLE_PERMISSION` VALUES ('e899fd8bebf24958b31fdc29485af439', '1', '14');

-- ----------------------------
-- Table structure for SYS_USER
-- ----------------------------
DROP TABLE IF EXISTS `SYS_USER`;
CREATE TABLE `SYS_USER`  (
  `ID` varchar(70) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `USERNAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名(登录使用)',
  `NAME` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '真实姓名',
  `PASSWORD` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `UPDATE_TIME` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `CREATE_TIME` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `VERSION` int(10) NULL DEFAULT NULL COMMENT '版本',
  `AVATAR` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像地址',
  `STATUS` int(10) NULL DEFAULT NULL COMMENT '状态 1启用 2 禁用',
  `MOBILE` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `EMAIL` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱地址',
  `GENDER` int(10) NULL DEFAULT NULL COMMENT '性别',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of SYS_USER
-- ----------------------------
INSERT INTO `SYS_USER` VALUES ('0e2dc764470d4dcbb65a045af0e279d7', 'shl', '张三', 'e10adc3949ba59abbe56e057f20f883e', NULL, '2018-12-06 10:13:26', 1, NULL, 1, NULL, NULL, NULL);
INSERT INTO `SYS_USER` VALUES ('1', 'admin', '管理员', 'e10adc3949ba59abbe56e057f20f883e', '2018-12-07 09:40:21', '2018-11-30 15:20:35', 12, '55,171b10d538', 1, '13420063161', '807745688@qq.com', 2);
INSERT INTO `SYS_USER` VALUES ('10a5a98864be41ddac7077d63ae443b2', 'gyz', '张四', 'e10adc3949ba59abbe56e057f20f883e', NULL, '2018-12-06 10:13:26', 1, NULL, 1, NULL, NULL, NULL);
INSERT INTO `SYS_USER` VALUES ('3c72aefabb754d94ad280c1759da2ba2', 'spj', '张五', 'e10adc3949ba59abbe56e057f20f883e', NULL, '2018-12-06 10:13:26', 1, NULL, 1, NULL, NULL, NULL);
INSERT INTO `SYS_USER` VALUES ('68ebcda15c434a94ae0b61313949b0ac', 'xzh', '张六', 'e10adc3949ba59abbe56e057f20f883e', NULL, '2018-12-06 10:13:26', 1, NULL, 1, NULL, NULL, NULL);
INSERT INTO `SYS_USER` VALUES ('8525e5823a504cb08441b8cf53b8da63', 'gsq', '张七', 'e10adc3949ba59abbe56e057f20f883e', NULL, '2018-12-06 10:13:26', 2, NULL, 2, NULL, NULL, NULL);
INSERT INTO `SYS_USER` VALUES ('9eb38ecc1d7345d9ba795ae6b3f33b27', 'wyh', '张八', 'e10adc3949ba59abbe56e057f20f883e', '2018-12-06 22:38:51', '2018-12-06 10:13:26', 6, NULL, 1, '13423243444', '12345672ewqe8@qq.com', 2);
INSERT INTO `SYS_USER` VALUES ('a23c4af53dbc4dfaa8c5232d3c6bf998', 'syn', '张九', 'e10adc3949ba59abbe56e057f20f883e', NULL, '2018-12-06 10:13:26', 2, NULL, 2, NULL, NULL, NULL);
INSERT INTO `SYS_USER` VALUES ('b1a1367bf39e4beb89cc1c031defb819', 'sgh', '张十', 'e10adc3949ba59abbe56e057f20f883e', NULL, '2018-12-06 10:13:26', 1, NULL, 1, NULL, NULL, NULL);
INSERT INTO `SYS_USER` VALUES ('b6c234a7b20f42889519e05e88742061', 'gsh', '李四', 'e10adc3949ba59abbe56e057f20f883e', NULL, '2018-12-06 10:13:26', 1, NULL, 1, NULL, NULL, NULL);
INSERT INTO `SYS_USER` VALUES ('c02af61c1ed243a68de03604d33d62f2', 'czq', '李五', 'e10adc3949ba59abbe56e057f20f883e', NULL, '2018-12-06 10:13:26', 1, NULL, 1, NULL, NULL, NULL);
INSERT INTO `SYS_USER` VALUES ('e220ee9de27e40e18988a646fc0a2f25', 'cwj', '李六', 'e10adc3949ba59abbe56e057f20f883e', NULL, '2018-12-06 10:13:26', 1, NULL, 1, NULL, NULL, NULL);
INSERT INTO `SYS_USER` VALUES ('f8e633ac5f694d78b164a623e81a8c6c', 'test', '测试人员', 'e10adc3949ba59abbe56e057f20f883e', NULL, '2018-12-03 22:06:58', 1, NULL, 1, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for SYS_USER_ROLE
-- ----------------------------
DROP TABLE IF EXISTS `SYS_USER_ROLE`;
CREATE TABLE `SYS_USER_ROLE`  (
  `ID` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ROLE_ID` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色id',
  `USER_ID` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of SYS_USER_ROLE
-- ----------------------------
INSERT INTO `SYS_USER_ROLE` VALUES ('aeba621e8684415dafb37920298efd1a', '1', '1');

-- ----------------------------
-- Table structure for SYS_USER_UPDATE_LOG
-- ----------------------------
DROP TABLE IF EXISTS `SYS_USER_UPDATE_LOG`;
CREATE TABLE `SYS_USER_UPDATE_LOG`  (
  `ID` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `USER_ID` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户id',
  `OPERATE_TIME` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  `OPERATE_PERSON` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作人 ',
  `OPERATE_CONTENT` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作内容',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of SYS_USER_UPDATE_LOG
-- ----------------------------
INSERT INTO `SYS_USER_UPDATE_LOG` VALUES ('175ada3b5dcb431fbecf3c1f31430309', '9eb38ecc1d7345d9ba795ae6b3f33b27', '2018-12-06 11:37:47', 'admin', '手机号由: ,修改为:13420069744;邮箱由: ,修改为:807758751@qq.com;');
INSERT INTO `SYS_USER_UPDATE_LOG` VALUES ('2cbdc1e5fe2640bdbb5f2824537c3a75', '1', '2018-12-07 09:14:54', 'admin', '性别由:女,修改为:男;');
INSERT INTO `SYS_USER_UPDATE_LOG` VALUES ('5e187b7f60194b8ea3663b0e77202dfa', '9eb38ecc1d7345d9ba795ae6b3f33b27', '2018-12-06 11:41:19', 'admin', '性别由:男,修改为:女;');
INSERT INTO `SYS_USER_UPDATE_LOG` VALUES ('64a97db2b0c04746b64289135d31547d', '1', '2018-12-06 22:49:19', 'admin', '手机号由:13420069744,修改为:13420063161;邮箱由:807758751@qq.com,修改为:807745688@qq.com;性别由:男,修改为:女;');
INSERT INTO `SYS_USER_UPDATE_LOG` VALUES ('6948a2dfb2754f3191543694bcb508f1', '9eb38ecc1d7345d9ba795ae6b3f33b27', '2018-12-06 22:38:51', 'admin', '手机号由:13423243567,修改为:13423243444;邮箱由:12345678@qq.com,修改为:12345672ewqe8@qq.com;');
INSERT INTO `SYS_USER_UPDATE_LOG` VALUES ('8c8707f02f2f45ccb682208ad6043b2c', '9eb38ecc1d7345d9ba795ae6b3f33b27', '2018-12-06 11:41:01', 'admin', '性别由:女,修改为:男;');
INSERT INTO `SYS_USER_UPDATE_LOG` VALUES ('c8e8a4967d8b4a538303033b5acd27e2', '9eb38ecc1d7345d9ba795ae6b3f33b27', '2018-12-06 22:34:04', 'admin', '手机号由:13420069744,修改为:13423243567;邮箱由:807758751@qq.com,修改为:12345678@qq.com;');
INSERT INTO `SYS_USER_UPDATE_LOG` VALUES ('d197c582f5444dc6a496d1bc7f6333e3', '1', '2018-12-06 22:35:31', 'admin', '手机号由: ,修改为:13420069744;邮箱由: ,修改为:807758751@qq.com;');
INSERT INTO `SYS_USER_UPDATE_LOG` VALUES ('ef5adcab719444b0b1252d0060aa7792', '1', '2018-12-07 09:20:59', 'admin', '性别由:男,修改为:女;');

-- ----------------------------
-- View structure for permissionlist
-- ----------------------------
DROP VIEW IF EXISTS `permissionlist`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `permissionlist` AS select `t`.`id` AS `pId`,`t`.`menu_name` AS `pLabel`,`child`.`ID` AS `cId`,`child`.`PERMISSION_NAME` AS `cLabel` from ((select (sum(`perm`.`ID`) + 10000) AS `id`,`perm`.`MENU_CODE` AS `menu_code`,`perm`.`MENU_NAME` AS `menu_name` from `SYS_PERMISSION` `perm` group by `perm`.`MENU_CODE`,`perm`.`MENU_NAME`) `t` left join `SYS_PERMISSION` `child` on((`child`.`MENU_CODE` = `t`.`menu_code`))) order by `child`.`ID`;

SET FOREIGN_KEY_CHECKS = 1;
