package ${package.Controller};


import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import com.base.util.Result;
import org.springframework.web.bind.annotation.RequestBody;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.Map;
import ${package.Service}.${table.serviceName};
import ${package.Entity}.${entity};
<#if restControllerStyle>
import org.springframework.web.bind.annotation.RestController;
<#else>
import org.springframework.stereotype.Controller;
</#if>
<#if superControllerClassPackage??>
import ${superControllerClassPackage};
</#if>

/**
 * <p>
 * ${table.comment!} 前端控制器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if restControllerStyle>
@RestController
<#else>
@Controller
</#if>
@RequestMapping("<#if package.ModuleName??>/${package.ModuleName}</#if>/<#if controllerMappingHyphenStyle??>${controllerMappingHyphen}<#else>${table.entityPath}</#if>")
<#if kotlin>
class ${table.controllerName}<#if superControllerClass??> : ${superControllerClass}()</#if>
<#else>
<#if superControllerClass??>
public class ${table.controllerName} extends ${superControllerClass} {
<#else>
public class ${table.controllerName} {
</#if>


  @Autowired
  private ${table.serviceName} ${table.serviceName?uncap_first};


  @RequestMapping("/list")
  public Result list(@RequestBody Map<String,Object> params){
        Integer page = Integer.valueOf(params.get("page").toString());
        Integer limit = Integer.valueOf(params.get("limit").toString());
        QueryWrapper<${entity}> queryWrapper=new QueryWrapper<${entity}>();
        Page<${entity}> pageRequest=new Page<${entity}>(page,limit);
        IPage<${entity}> iPage = ${table.serviceName?uncap_first}.page(pageRequest, queryWrapper);
        return Result.successResult(iPage);
  }


  @RequestMapping("/save-or-update")
  public Result saveOrUpdate(@RequestBody ${entity} ${entity?uncap_first}){
       boolean b = ${table.serviceName?uncap_first}.saveOrUpdate(${entity?uncap_first});
        if(!b) {
            throw new RuntimeException();
        }
        return Result.successResult();
  }

    <#list table.fields as field>
        <#if field.keyFlag>
            <#assign propertyType="${field.propertyType}"/>
        </#if>
    </#list>

  @RequestMapping("/delete")
  public Result delete(@RequestBody List<${propertyType}> list){
        boolean b= ${table.serviceName?uncap_first}.removeByIds(list);
        if(!b){
            throw new RuntimeException();
        }
        return Result.successResult();
    }




}
</#if>
