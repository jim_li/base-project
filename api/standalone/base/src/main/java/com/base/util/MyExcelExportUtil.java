package com.base.util;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MyExcelExportUtil  {

    protected static XSSFWorkbook exportWorkbook(List<?> dataList) {
        if(dataList != null && dataList.size() != 0) {
            Class sheetClass = dataList.get(0).getClass();
            ExcelSheet excelSheet = (ExcelSheet)sheetClass.getAnnotation(ExcelSheet.class);
            String sheetName = dataList.get(0).getClass().getSimpleName();
            HSSFColor.HSSFColorPredefined headColor = null;
            if(excelSheet != null) {
                if(excelSheet.value() != null && excelSheet.value().trim().length() > 0) {
                    sheetName = excelSheet.value().trim();
                }

                headColor = excelSheet.headColor();
            }

            ArrayList fields = new ArrayList();
            if(sheetClass.getDeclaredFields() != null && sheetClass.getDeclaredFields().length > 0) {
                Field[] workbook = sheetClass.getDeclaredFields();
                int sheet = workbook.length;

                for(int headStyle = 0; headStyle < sheet; ++headStyle) {
                    Field headRow = workbook[headStyle];
                    ExcelField rowData = (ExcelField)headRow.getAnnotation(ExcelField.class);
                    if(rowData==null){
                        continue;
                    }
                    String name = rowData.value().trim();
                    if(StringUtils.isEmpty(name)){
                        continue;
                    }
                    if(!Modifier.isStatic(headRow.getModifiers())) {
                        fields.add(headRow);
                    }
                }
            }

            if(fields != null && fields.size() != 0) {
                XSSFWorkbook var19 = new XSSFWorkbook();
                Sheet var20 = var19.createSheet(sheetName);
                CellStyle var21 = null;
                if(headColor != null) {
                    var21 = var19.createCellStyle();
                    var21.setFillForegroundColor(headColor.getIndex());
                    var21.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                    var21.setFillBackgroundColor(headColor.getIndex());
                }

                Row var22 = var20.createRow(0);

                int dataIndex;
                for(dataIndex = 0; dataIndex < fields.size(); ++dataIndex) {
                    Field rowIndex = (Field)fields.get(dataIndex);
                    ExcelField rowData = (ExcelField)rowIndex.getAnnotation(ExcelField.class);
                    if(rowData==null){
                        continue;
                    }
                    String name = rowData.value().trim();
                    if(StringUtils.isEmpty(name)){
                        continue;
                    }
                    String rowX = rowData != null && rowData.value() != null && rowData.value().trim().length() > 0?rowData.value():rowIndex.getName();
                    Cell i = var22.createCell(dataIndex, 1);
                    if(var21 != null) {
                        i.setCellStyle(var21);
                    }

                    i.setCellValue(String.valueOf(rowX));
                }

                for(dataIndex = 0; dataIndex < dataList.size(); ++dataIndex) {
                    int var23 = dataIndex + 1;
                    Object var24 = dataList.get(dataIndex);
                    Row var25 = var20.createRow(var23);

                    for(int var26 = 0; var26 < fields.size(); ++var26) {
                        Field field = (Field)fields.get(var26);
                        ExcelField rowData = (ExcelField)field.getAnnotation(ExcelField.class);
                        if(rowData==null){
                            continue;
                        }
                        String name = rowData.value().trim();
                        if(StringUtils.isEmpty(name)){
                            continue;
                        }
                        try {
                            field.setAccessible(true);
                            Object e = field.get(var24);
                            Cell cellX = var25.createCell(var26, 1);
                            if(e==null){
                                cellX.setCellValue("");
                            }else{
                                Class<?> aClass = e.getClass();
                                Date date=new Date();
                                if(aClass == date.getClass()){
                                    cellX.setCellValue(DateUtils.format((Date) e,DateUtils.DATE_TIME_PATTERN));
                                }else{
                                    cellX.setCellValue(String.valueOf(e));
                                }
                            }
                        } catch (IllegalAccessException var18) {
                            throw new RuntimeException(var18);
                        }
                    }
                }

                return var19;
            } else {
                throw new RuntimeException(">>>>>>>>>>> xxl-excel error, data field can not be empty.");
            }
        } else {
            throw new RuntimeException(">>>>>>>>>>> xxl-excel error, data can not be empty.");
        }
    }
}
