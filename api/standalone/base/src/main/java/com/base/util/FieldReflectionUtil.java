package com.base.util;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class FieldReflectionUtil {
    private FieldReflectionUtil() {
    }

    public static Byte parseByte(String value) {
        try {
            value = value.replaceAll("　", "");
            return Byte.valueOf(value);
        } catch (NumberFormatException var2) {
            throw new RuntimeException("parseByte but input illegal input=" + value, var2);
        }
    }

    public static Boolean parseBoolean(String value) {
        value = value.replaceAll("　", "");
        if (Boolean.TRUE.toString().equalsIgnoreCase(value)) {
            return Boolean.TRUE;
        } else if (Boolean.FALSE.toString().equalsIgnoreCase(value)) {
            return Boolean.FALSE;
        } else {
            throw new RuntimeException("parseBoolean but input illegal input=" + value);
        }
    }

    public static Integer parseInt(String value) {
        try {
            value = value.replaceAll("　", "");
            return Integer.valueOf(value);
        } catch (NumberFormatException var2) {
            throw new RuntimeException("parseInt but input illegal input=" + value, var2);
        }
    }

    public static Short parseShort(String value) {
        try {
            value = value.replaceAll("　", "");
            return Short.valueOf(value);
        } catch (NumberFormatException var2) {
            throw new RuntimeException("parseShort but input illegal input=" + value, var2);
        }
    }

    public static Long parseLong(String value) {
        try {
            value = value.replaceAll("　", "");
            return Long.valueOf(value);
        } catch (NumberFormatException var2) {
            throw new RuntimeException("parseLong but input illegal input=" + value, var2);
        }
    }

    public static Float parseFloat(String value) {
        try {
            value = value.replaceAll("　", "");
            return Float.valueOf(value);
        } catch (NumberFormatException var2) {
            throw new RuntimeException("parseFloat but input illegal input=" + value, var2);
        }
    }

    public static Double parseDouble(String value) {
        try {
            value = value.replaceAll("　", "");
            return Double.valueOf(value);
        } catch (NumberFormatException var2) {
            throw new RuntimeException("parseDouble but input illegal input=" + value, var2);
        }
    }

    public static Date parseDate(String value) {
        try {
            String datePattern = "yyyy-MM-dd HH:mm:ss";
            SimpleDateFormat dateFormat = new SimpleDateFormat(datePattern);
            return dateFormat.parse(value);
        } catch (ParseException var3) {
            throw new RuntimeException("parseDate but input illegal input=" + value, var3);
        }
    }

    public static Object parseValue(Field field, String value) {
        Class<?> fieldType = field.getType();
        if (value != null && value.trim().length() != 0) {
            value = value.trim();
            if (!Byte.class.equals(fieldType) && !Byte.TYPE.equals(fieldType)) {
                if (!Boolean.class.equals(fieldType) && !Boolean.TYPE.equals(fieldType)) {
                    if (String.class.equals(fieldType)) {
                        return value;
                    } else if (!Short.class.equals(fieldType) && !Short.TYPE.equals(fieldType)) {
                        if (!Integer.class.equals(fieldType) && !Integer.TYPE.equals(fieldType)) {
                            if (!Long.class.equals(fieldType) && !Long.TYPE.equals(fieldType)) {
                                if (!Float.class.equals(fieldType) && !Float.TYPE.equals(fieldType)) {
                                    if (!Double.class.equals(fieldType) && !Double.TYPE.equals(fieldType)) {
                                        if (Date.class.equals(fieldType)) {
                                            return parseDate(value);
                                        } else {
                                            throw new RuntimeException("request illeagal type, type must be Integer not int Long not long etc, type=" + fieldType);
                                        }
                                    } else {
                                        return parseDouble(value);
                                    }
                                } else {
                                    return parseFloat(value);
                                }
                            } else {
                                return parseLong(value);
                            }
                        } else {
                            return parseInt(value);
                        }
                    } else {
                        return parseShort(value);
                    }
                } else {
                    return parseBoolean(value);
                }
            } else {
                return parseByte(value);
            }
        } else {
            return null;
        }
    }
}
