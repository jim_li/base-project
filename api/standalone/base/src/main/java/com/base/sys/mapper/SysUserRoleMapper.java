package com.base.sys.mapper;

import com.base.sys.entity.SysUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author gjj
 * @since 2018-11-27
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
