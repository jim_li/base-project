package com.base.sys.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.base.sys.dto.SysUserQueryDTO;
import com.base.sys.dto.UserMenuPermissionDTO;
import com.base.sys.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;
import com.base.util.Result;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author gjj
 * @since 2018-11-23
 */
public interface ISysUserService extends IService<SysUser> {


    /**
     * 获取用户的菜单和权限
     * @param id
     * @return
     */
    UserMenuPermissionDTO getUserMenuPermission(String id);


    IPage<SysUser> selectPageNew(Page page, Wrapper wrapper);

    List<SysUser> selectNew( Wrapper wrapper);

    Result assignRole(SysUser webUser);
}
