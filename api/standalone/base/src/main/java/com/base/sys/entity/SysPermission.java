package com.base.sys.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author gjj
 * @since 2018-11-27
 */
@TableName("SYS_PERMISSION")
public class SysPermission implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 自定id,主要供前端展示权限列表分类排序使用.
     */
    @TableId("ID")
    private Integer id;

    /**
     * 归属菜单,前端判断并展示菜单使用,
     */
    @TableField("MENU_CODE")
    private String menuCode;

    /**
     * 菜单的中文释义
     */
    @TableField("MENU_NAME")
    private String menuName;

    /**
     * 权限的代码/通配符,对应代码中@RequiresPermissions 的value
     */
    @TableField("PERMISSION_CODE")
    private String permissionCode;

    /**
     * 本权限的中文释义
     */
    @TableField("PERMISSION_NAME")
    private String permissionName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public String getMenuCode() {
        return menuCode;
    }

    public void setMenuCode(String menuCode) {
        this.menuCode = menuCode;
    }
    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }
    public String getPermissionCode() {
        return permissionCode;
    }

    public void setPermissionCode(String permissionCode) {
        this.permissionCode = permissionCode;
    }
    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    @Override
    public String toString() {
        return "SysPermission{" +
        "id=" + id +
        ", menuCode=" + menuCode +
        ", menuName=" + menuName +
        ", permissionCode=" + permissionCode +
        ", permissionName=" + permissionName +
        "}";
    }
}
