package com.base.sys.dto;

import com.base.util.PageQuery;

public class SysDictQueryDTO extends PageQuery {

    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
